import org.firmata.*;
import cc.arduino.*;
import processing.serial.*;

boolean test = false;
boolean loop = false;

/////////////////////////////////
//Encoder

Serial myPort;  // Create object from Serial class
int val;      // Data received from the serial port
int previousVal; 
int refDistance  = 15;
int dist = 15;

/////////////////////////////////
String leTexte = "place2/TeenageXport_SEGPA_07.csv";

int delay1 = 100;
int delay2 = 300;


/////////////////////////////////
Arduino arduino;

PFont f;
int nbrDotsPerLine = 5;
int cellSize =100;
int ligneAImprimer = 0;
// PARAMS
char[] lesPoints = new char[5];

ArrayList<Texte> serietextes = new ArrayList<Texte>();
int maxY = 0;

int idTexte = 0;

//ENABLE = 6
int ENA_PWM = 6;

//-- MOTEUR 1 --
int IN11 = 11;
int IN12 = 7;

//-- MOTEUR 2 --
int IN21 = 2;
int IN22 = 3;

//-- MOTEUR 3 --
int IN31 = 4;
int IN32 = 5;

//-- MOTEUR 4 --
int IN41 = 12;
int IN42 = 13;

//-- MOTEUR 5 --
int IN51 = 8;
int IN52 = 9;

PVector motorArray[] = new PVector[5];


public void setup() {
  size( 500, 300);
  background(100);
  stroke(255);
  fill( 0 );
  smooth();
  createSerieTextes();
  //
  if (test == false)
  {
    println(Arduino.list());
    arduino = new Arduino(this, "/dev/ttyUSB0", 57600);

    //Encoder
    printArray(Serial.list());
    String portName = Serial.list()[4];
    myPort = new Serial(this, "/dev/ttyUSB1", 115200);
    myPort.bufferUntil('\n');

    //ENABLE
    arduino.pinMode(ENA_PWM, Arduino.OUTPUT);

    arduino.pinMode(IN11, Arduino.OUTPUT);
    arduino.pinMode(IN12, Arduino.OUTPUT);
    arduino.pinMode(IN21, Arduino.OUTPUT);
    arduino.pinMode(IN22, Arduino.OUTPUT);
    arduino.pinMode(IN31, Arduino.OUTPUT);
    arduino.pinMode(IN32, Arduino.OUTPUT);
    arduino.pinMode(IN41, Arduino.OUTPUT);
    arduino.pinMode(IN42, Arduino.OUTPUT);
    arduino.pinMode(IN51, Arduino.OUTPUT);
    arduino.pinMode(IN52, Arduino.OUTPUT);
  }

  motorArray[0] = new PVector(IN11, IN12);
  motorArray[1] = new PVector(IN21, IN22);
  motorArray[2] = new PVector(IN31, IN32);
  motorArray[3] = new PVector(IN41, IN42);
  motorArray[4] = new PVector(IN51, IN52);

  f = createFont("Georgia", 16);
  textFont(f);
  textAlign(CENTER, CENTER);
}

public void draw() {
  background (100);
  fill(255, 0, 0);
  noStroke();
  rect(0, 100, width, 100);
  fill(255);
  text("Ligne n°"+ligneAImprimer, 60, 20);
  if (ligneAImprimer >0) printLine(ligneAImprimer-1, 50, false);
  fill(255);
  text("Ligne n°"+((int)ligneAImprimer+1), 60, 120);
  printLine(ligneAImprimer, 150, true);
  fill(255);
  text("Ligne n°"+((int)ligneAImprimer+2), 60, 220);
  if (ligneAImprimer < maxY) 
  {
    printLine(ligneAImprimer+1, 250, false);
  }
}

void createSerieTextes() {
  serietextes.add(new Texte(leTexte));
}

void printLine(int j, int leY, boolean drawing) {

  Texte monTexte = serietextes.get(idTexte);

  Table table = monTexte.getTable();
  maxY = monTexte.getNumberOfRows()-1;
  TableRow row = table.getRow(j);


  String[] cellsContent = new String[5];
  cellsContent[0] = row.getString("col0");
  cellsContent[1] = row.getString("col1");
  cellsContent[2]  = row.getString("col2");
  cellsContent[3] = row.getString("col3");
  cellsContent[4]  = row.getString("col4");

  for (int k=0; k<nbrDotsPerLine; k++)
  {
    stroke(255);
    int X1 = k*cellSize;
    int Y1 = j*cellSize;
    int X2 = (k+1)*cellSize;
    int Y2 = j*cellSize;
    //
    line (X1, leY, X2, leY);
    ellipse(X1+cellSize/2, leY, 2, 2);
    //println(cellsContent[k]);
    if (cellsContent[k].equals("X"))
    {
      fill(0);
      noStroke();
      ellipse(X1+cellSize/2, leY, 10, 10);
      if (drawing) { 
        lesPoints[k] = 1;
      }
    } else {

      if (drawing) lesPoints[k] = 0;
    }
  }
}




void pshiit_1(int motor)
{
  println(motor);
  if (test == false)
  {
    arduino.analogWrite(ENA_PWM, 200);
    arduino.digitalWrite((int)motorArray[motor].x, Arduino.LOW);
    arduino.digitalWrite((int)motorArray[motor].y, Arduino.HIGH);
  }
}

void pshiit_2(int motor)
{
  if (test == false)
  {
    arduino.digitalWrite((int)motorArray[motor].x, Arduino.HIGH);
    arduino.digitalWrite((int)motorArray[motor].y, Arduino.LOW);
  }
}

void pshiit_3(int motor)
{
  if (test == false)
  {
    arduino.analogWrite(ENA_PWM, 0);
    arduino.digitalWrite((int)motorArray[motor].x, Arduino.LOW);
    arduino.digitalWrite((int)motorArray[motor].y, Arduino.LOW);
  }
}


void printLine() {
  println(lesPoints);
  for (int i=0; i<5; i++)
  {
    if (lesPoints[i]==1) pshiit_1(i);
  }
  delay(delay1);
  for (int i=0; i<5; i++)
  {
    pshiit_2(i);
  }
  delay(delay2);
  for (int i=0; i<5; i++)
  {
    pshiit_3(i);
  }

  if (ligneAImprimer<maxY)
  {
    ligneAImprimer++;
  } else
    if (loop)
    {
      ligneAImprimer = 0;
    }
}

void keyPressed()
{
  if (key == CODED) {
    if (keyCode == DOWN && ligneAImprimer<maxY)ligneAImprimer++;
    if (keyCode == UP && ligneAImprimer>0)ligneAImprimer--;
  }
  if (key == ' ')printLine();
  if (key == '1')pshiit(0);
  if (key == '2')pshiit(1);
  if (key == '3')pshiit(2);
  if (key == '4')pshiit(3);
  if (key == '5')pshiit(4);
  if (key == 'z')dist = refDistance;
}

void pshiit(int motor)
{
  println(motor);
  if (test == false)
  {
    arduino.analogWrite(ENA_PWM, 200);
    arduino.digitalWrite((int)motorArray[motor].x, Arduino.LOW);
    arduino.digitalWrite((int)motorArray[motor].y, Arduino.HIGH);
    delay(delay1);
    arduino.digitalWrite((int)motorArray[motor].x, Arduino.HIGH);
    arduino.digitalWrite((int)motorArray[motor].y, Arduino.LOW);
    delay(delay2);
    arduino.analogWrite(ENA_PWM, 0);
    arduino.digitalWrite((int)motorArray[motor].x, Arduino.LOW);
    arduino.digitalWrite((int)motorArray[motor].y, Arduino.LOW);
  }
}

void serialEvent(Serial myPort) {

  String myString = myPort.readStringUntil('\n');
  if (myString != null) {
    try {
      val=Integer.parseInt(myString.trim());
      if (val !=  previousVal)
      {
        int diff = val-previousVal;
        if (diff == 127) diff = -1;
        if (diff == -127) diff =1;
        dist += diff;
        if (dist == 0)
        {
          println("pschiit");
          printLine();
          dist = refDistance;
        }
        println(dist);
      }
    } 
    catch (NumberFormatException npe) {
      // Not an integer so forget it
    }
  }
  previousVal = val;
}