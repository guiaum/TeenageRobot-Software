import processing.core.PVector;
import java.util.ArrayList;
import java.util.List;


public class Texte {

  String myPath;
  Table table;
  boolean hasBeenGenerated = false;
  int lastRow=0;

  List<PVector> arrayCell = new ArrayList<PVector>();

  public Texte(String _myPath)
  {
    myPath = _myPath;
    //Création d'un nouveau texte
    table = loadTable( _myPath, "header" );
    if (loop)
    {
      TableRow row = table.addRow();
      for (int i=0; i<nbrDotsPerLine; i++)
      {
        //Add a blank line for loop
        row.setString("col"+i, "");
      }
    }
    hasBeenGenerated = true;
  }

  public Table getTable()
  {
    return table;
  }


  public int getNumberOfRows() {
    return table.getRowCount();
  }

  public Boolean hasBeenGenerated()
  {
    return hasBeenGenerated;
  }
}