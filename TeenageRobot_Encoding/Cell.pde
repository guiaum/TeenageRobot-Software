class Cell 
{
  float cellWidth;  
  float cellHeight; 
  float cellX; 
  float cellY; 
  PImage myArea;
  int score = 0;
  boolean dot = false;
  int Id;
  
  int localAdjustWidth = 1;
  int localAdjustHeight = 1;
  
  Cell(float pCellWidth, float pCellHeight, float pCellX, float pCellY, int pId) {
    cellWidth = pCellWidth;  
    cellHeight = pCellHeight; 
    cellX = pCellX; 
    cellY = pCellY;
    Id = pId;
  }

  void display() {
    stroke(255,0,0,127);
    if (score/1000>scoreLimit)
    { 
      noFill();
      dot = false;
    }else{
      fill(0,255,0,127);
      dot = true;
    }
    strokeWeight(1);
    rect(cellX - localAdjustWidth , cellY - localAdjustHeight, cellWidth+( localAdjustWidth * 2), cellHeight + (localAdjustHeight * 2));
    
    //Debug
    //fill(0);
    //text(Id, cellX+5, cellY+15);
    
  }
  
  void loadArea(PImage pMyArea)
  {
    score = 0;
    myArea = pMyArea;
    int dim = myArea.width * myArea.height;
    myArea.loadPixels();
    for (int i = 0; i < dim; i++) {
      score += brightness(myArea.pixels[i]) ;
    }
    myArea.updatePixels();
    //println ("X "+cellX + " Y "+cellY + " Score: " + score);
  }
  
  void adjust(int plocalAdjustWidth, int plocalAdjustHeight)
  {
    localAdjustWidth = plocalAdjustWidth;
    localAdjustHeight = plocalAdjustHeight;
  }
  
  float getX()
  {
    return cellX - localAdjustWidth;
  }
  
  float getY()
  {
    return cellY - localAdjustHeight;
  }
  
  float getWidth()
  {
    return cellWidth +( localAdjustWidth * 2);
  }
  
  float getHeight()
  {
    return cellHeight + (localAdjustHeight * 2);
  }
  
  boolean getDot()
  {
   return dot; 
  }
    
  
}