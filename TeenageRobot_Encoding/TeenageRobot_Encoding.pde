import gab.opencv.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.core.Mat;
import org.opencv.core.CvType;
import controlP5.*;
import com.thomasdiewald.ps3eye.PS3EyeP5;

//TO do
// better scoring scale (15 -> 900)

PS3EyeP5 ps3eye;

ControlP5 cp5;
Textlabel myTextlabelB;

Table table;

//////////////////////////////////////
int state = 0;
int mode = 0;
int indexContour=0;
int threshold_Value = 110;
float threshold_Value2 = 0.5;
float theScale = 1.5;
int scoreLimit = 400000;
boolean invert = false;
int numberOfDots = 5;
int maxRows; // Depends on the pattern
int totalRows;
int limRows; 
int nbrOfLines;
float sizeOfLine;
float cellWidth;
float cellHeight;
int adjustWidth = 0;
int adjustHeight = 0;
//////////////////////////////////////
OpenCV opencv;
PImage src;
PImage card;
int cardWidth = int(370*theScale);
int cardHeight = int(250*theScale);
int camWidth = 640;
int camHeight = 480;
//////////////////////////////////////

ArrayList<Cell> myCells = new ArrayList();

Contour contour;

void setup() {

  size(1195, 580, P2D);

  ////////////////////////////////////// 
  //CAMERA
  ps3eye = PS3EyeP5.getDevice(this);
  if (ps3eye == null) {
    System.out.println("No PS3Eye connected. Good Bye!");
    exit();
    return;
  }
  ps3eye.start();
  //////////////////////////////////////
  //OPENCV
  opencv = new OpenCV(this, camWidth, camHeight);
  card = createImage(cardWidth, cardHeight, ARGB); 
  //ps3eye.setExposure(18);
  ps3eye.setRedBalance(0);

  //////////////////////////////////////
  int offsetX;
  int offsetY;
  switch (mode)
  {
  case 0:
    //5x5 Matrix mode
    //Start X91 Y25
    //Cell 40*40
    maxRows = 5;
    nbrOfLines = 1;
    sizeOfLine = 200; // Don't care
    totalRows = maxRows*nbrOfLines;
    offsetX = int(91*theScale);
    cellWidth = 40*theScale;
    cellHeight = 40*theScale;

    for (int k = 0; k < nbrOfLines; k++)
    {
      offsetY = int(25*theScale) + int((sizeOfLine*theScale)*k);
      
      for (int i=0; i<maxRows; i++)
      {
        for (int j=0; j<numberOfDots; j++)
        {
          Cell myCell = new Cell(cellWidth, cellHeight, (i*cellWidth)+ offsetX, (j*cellHeight) + offsetY, (i*maxRows)+j );
          myCells.add(myCell);
          println (i+" "+j+" "+k+" "+offsetY);
        }
      }
    }
    break;
    
    case 1:
    //19x5 * 2 lines Matrix mode
    //Start X28.2 Y33.4
    //Cell 17.9*19.3
    maxRows = 19;
    nbrOfLines = 2;
    sizeOfLine = 109;
    totalRows = maxRows*nbrOfLines;
    offsetX = int(19*theScale);
    cellWidth = 17.5*theScale;
    cellHeight = 18.5*theScale;

    for (int k = 0; k < nbrOfLines; k++)
    {
      offsetY = int(25*theScale) + int((sizeOfLine*theScale)*k);
      
      for (int i=0; i<maxRows; i++)
      {
        for (int j=0; j<numberOfDots; j++)
        {
          Cell myCell = new Cell(cellWidth, cellHeight, (i*cellWidth)+ offsetX, (j*cellHeight) + offsetY, (i*maxRows)+j );
          myCells.add(myCell);
          println (i+" "+j+" "+k+" "+offsetY);
        }
      }
    }
    break;  
  
  case 2:
    //38x5 * 4 lines Matrix mode
    //Start X18 Y16
    //Cell 8.8*9.6
    maxRows = 38;
    nbrOfLines = 4;
    sizeOfLine = 57;
    totalRows = maxRows*nbrOfLines;
    offsetX = int(18*theScale);
    cellWidth = 8.8*theScale;
    cellHeight = 9.6*theScale;

    for (int k = 0; k < nbrOfLines; k++)
    {
      offsetY = int(16*theScale) + int((sizeOfLine*theScale)*k);
      
      for (int i=0; i<maxRows; i++)
      {
        for (int j=0; j<numberOfDots; j++)
        {
          Cell myCell = new Cell(cellWidth, cellHeight, (i*cellWidth)+ offsetX, (j*cellHeight) + offsetY, (i*maxRows)+j );
          myCells.add(myCell);
          println (i+" "+j+" "+k+" "+offsetY);
        }
      }
    }
    break;
  }


  //////////////////////////////////////
  //GUI
  cp5 = new ControlP5(this);
  //LEFT
  cp5.addSlider("threshold_Value")
    .setPosition(20, 480 + 20)
    .setRange(0, 255)
    ;

  // To navigate through contour array
  cp5.addButton("Previous_Contour")
    .setPosition(20, 480 + 40)
    .setSize(200, 19)
    ;

  cp5.addButton("Next_Contour")
    .setPosition(20, 480 + 60)
    .setSize(200, 19)
    ;

  // in case image is flipped
  cp5.addToggle("invert")
    .setPosition(640 -20, 480 + 20)
    .setSize(20, 20)
    ;

  //RIGHT
  cp5.addSlider("limRows")
    .setPosition(camWidth + 20, cardHeight + 20)
    .setWidth(cardWidth - 70)
    .setRange(1, totalRows)
    .setSliderMode(Slider.FLEXIBLE)
    .setValue(totalRows)
    ;

  cp5.addSlider("threshold_Value2")
    .setPosition(camWidth + 20, cardHeight + 50)
    .setWidth(250)
    .setRange(0, 1)
    ;

  cp5 = new ControlP5(this);
  cp5.addSlider("scoreLimit")
    .setPosition(camWidth + 20, cardHeight + 70)
    .setWidth(cardWidth - 70)
    .setRange(0, 900)
    ;

  cp5 = new ControlP5(this);
  cp5.addSlider("adjustWidth")
    .setPosition(camWidth + 20, cardHeight + 90)
    .setWidth(cardWidth - 70)
    .setRange(-3, 3)
    ;

  cp5 = new ControlP5(this);
  cp5.addSlider("adjustHeight")
    .setPosition(camWidth + 20, cardHeight + 110)
    .setWidth(cardWidth - 70)
    .setRange(-3, 3)
    ;


  // Name of file
  cp5.addTextfield("Name")
    .setPosition(width - 500, 480 + 40)
    .setAutoClear(false)
    ;

  // Xport to CSV
  cp5.addButton("xportToCSV")
    .setPosition(width - 250, 480+40)
    .setSize(200, 19)
    ;

  myTextlabelB = new Textlabel(cp5, "", width - 250, 480+70, 400, 200);
}

void draw() {
  background(0);
  switch (state)
  {
  case 0:
    if (ps3eye.isAvailable()) {
      image(ps3eye.getFrame(), 0, 0, camWidth, camHeight);
      opencv.loadImage(ps3eye.getFrame());
      //Detect Main Edge
      opencv.threshold(threshold_Value);
      if ( opencv.findContours(true, true).size()>1)
      {
        contour = opencv.findContours(true, true).get(indexContour).getPolygonApproximation();
        noFill(); 
        stroke(0, 255, 0); 
        strokeWeight(4);
        contour.draw();
        if (contour.getPoints().size()==4)
        {
          opencv.toPImage(warpPerspective(contour.getPoints(), cardWidth, cardHeight), card);
        }
      }
    } else {
      // to avoid glitch
      image(ps3eye.getFrame(), 0, 0, camWidth, camHeight);
    }
    // Card to screen
    pushMatrix();
    translate(camWidth, 0);
    fill(127);
    rect(0, 0, cardWidth, cardHeight);
    if (invert)
    {
      PImage flippedCard = card.copy();
      card.loadPixels();
      // Begin loop for width
      for (int i = 0; i < card.width; i++) {
        // Begin loop for height
        for (int j = 0; j < card.height; j++) {    
          flippedCard.pixels[j*card.width+i] = card.pixels[(card.width - i - 1) + j*card.width]; // Reversing x to mirror the image
        }
      }
      card.updatePixels();
      card = flippedCard.copy();
    }
    image(card, 0, 0);
    card.filter(THRESHOLD, threshold_Value2);

    for (int i = 0; i<limRows*numberOfDots; i++)
    {
      Cell myCell = myCells.get(i);
      myCell.adjust(adjustWidth, adjustHeight);
      myCell.display();
    }
    //Analyze each part if the image
    loadCells();
    popMatrix();

    break;
  }

  myTextlabelB.draw(this);
}

void keyPressed()
{
  if (key == ' ')xportToCSV();
}

void loadCells()
{
  for (int i = 0; i<myCells.size(); i++)
  {
    Cell myCell = myCells.get(i);
    PImage myArea = card.get(int(myCell.getX()), int(myCell.getY()), int(myCell.getWidth()), int(myCell.getHeight()));
    myCell.loadArea(myArea);
  }
}

void xportToCSV()
{
  table = new Table();
  table.addColumn("col4");
  table.addColumn("col3");
  table.addColumn("col2");
  table.addColumn("col1");
  table.addColumn("col0");

  //for (int i=0; i<limRows; i++)
  //{
  //  TableRow newRow = table.addRow();
  //  for (int j=0; j<numberOfDots; j++)
  //  {
  //    Cell myCell = myCells.get((i*limRows)+j);
  //    if (myCell.getDot())
  //    {
  //      // Invert col
  //      int invertedCol = numberOfDots-1-j;
  //      newRow.setString("col"+str(invertedCol), "X");
  //    }
  //  }
  //}

  for (int j=0; j<limRows; j++)
  {
    table.addRow();
  }

  for (int i=0; i<limRows*numberOfDots; i++)
  {
    int row = i/numberOfDots;
    Cell myCell = myCells.get(i);
    if (myCell.getDot())
    {
      // Invert col
      //int invertedCol = numberOfDots-1-j;
      table.getRow(row).setString("col"+str(i%numberOfDots), "X");
    }
  }



  saveTable(table, "data/TeenageXport_"+  cp5.get(Textfield.class, "Name").getText()+".csv");
  myTextlabelB.setText("Last Xport: TeenageXport_"+cp5.get(Textfield.class, "Name").getText()+".csv" );
}

void Previous_Contour()
{
  if (indexContour !=0) indexContour--;
}

void Next_Contour()
{
  indexContour++;
}

Mat getPerspectiveTransformation(ArrayList<PVector> inputPoints, int w, int h) {
  Point[] canonicalPoints = new Point[4];
  canonicalPoints[0] = new Point(w, 0);
  canonicalPoints[1] = new Point(0, 0);
  canonicalPoints[2] = new Point(0, h);
  canonicalPoints[3] = new Point(w, h);
  MatOfPoint2f canonicalMarker = new MatOfPoint2f();
  canonicalMarker.fromArray(canonicalPoints);

  Point[] points = new Point[4];
  for (int i = 0; i < 4; i++) {
    points[i] = new Point(inputPoints.get(i).x, inputPoints.get(i).y);
  }
  MatOfPoint2f marker = new MatOfPoint2f(points);
  return Imgproc.getPerspectiveTransform(marker, canonicalMarker);
}

Mat warpPerspective(ArrayList<PVector> inputPoints, int w, int h) {
  Mat transform = getPerspectiveTransformation(inputPoints, w, h);
  Mat unWarpedMarker = new Mat(w, h, CvType.CV_8UC1);    
  Imgproc.warpPerspective(opencv.getColor(), unWarpedMarker, transform, new Size(w, h));
  return unWarpedMarker;
}